~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1)Installed nginx ingress, grafana and prometheus from helm charts

The Helm repos:


1. Ingress installation: 

helm repo add nginx-stable https://helm.nginx.com/stable 
helm update 
helm install my-ingress nginx-stable/nginx-ingress \
--namespace ingress \
--set controller.metrics.enabled=true \
--set controller.metrics.serviceMonitor.enabled=true \
--set controller.metrics.serviceMonitor.additionalLabels.release="prometheus"

2. Grafana installation: 
helm repo add grafana https://grafana.github.io/helm-charts 
helm update 
helm install my-grafana grafana/grafana 
Grafana's:
- username: admin 
- Password: UrN1xZVn0nnP6xBCvSFLceggfPFq8TTCVcO4rDLf


3. Prometheus installation: 
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts 
helm update 
helm install my-prometheus prometheus-community/prometheus \
--namespace monitoring  \
--set prometheus.prometheusSpec.podMonitorSelectorNilUsesHelmValues=false \
--set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
2)Created sample application with error port: 

kubectl create deployment demo --image=httpd --port=80 
kubectl expose deployment demo 
kubectl create ingress demo-localhost  --class=nginx --rule="testapp.kz/*=demo:3000" 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

3)In the repository you have 2 bash scripts, which one is called "hosts.sh", its will tee 
the domains to your /etc/hosts, and the second bash script "curl-script.sh"  
will load to the demo apllication http requests 

Alerts in the repo png pictures. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

